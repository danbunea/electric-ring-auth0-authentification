(ns auth.config)

(def auth0-config
  {:domain "..."
   :client-id "..."
   :client-secret "..."

   :auth-uri "/oauth/token"
   :redirect-uri "http://localhost:8081/auth/code"})

