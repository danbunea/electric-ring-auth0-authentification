(ns auth.auth-domain
  (:require
   [spy.core :as spy]
   [hyperfiddle.rcf :refer [tests]]))

(defn create-auth0-login-url [config state]
  (let [auth0-domain (:domain config)
        client-id (:client-id config)
        redirect-uri (:redirect-uri config)
        response-type "code"
        scope "openid profile" ; Adjust the scope as needed
        ]
    (str "https://" auth0-domain "/authorize?"
         "response_type=" response-type "&"
         "client_id=" client-id "&"
         "redirect_uri=" (java.net.URLEncoder/encode redirect-uri "UTF-8") "&"
         "scope=" (java.net.URLEncoder/encode scope "UTF-8") "&"
         "state=" state)))

(tests
 (def state (str (java.util.UUID/randomUUID)))
 (create-auth0-login-url {:domain "domain"
                          :client-id :client-id
                          :client-secret :client-secret
                          :redirect-uri "http://localhost:8080/auth-code"}
                         state)
 *1 := (str "https://domain/authorize?response_type=code&client_id=:client-id&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fauth-code&scope=openid+profile&state=" state))

(comment

  (create-auth0-login-url auth.config/auth0-config (str (java.util.UUID/randomUUID)))

  nil)