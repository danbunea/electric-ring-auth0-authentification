(ns auth.use-cases.token-from-code.auth0
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [clj-http.client :as client]
   [spy.core :as spy]))

(defn- unexceptional-status [code] (<= 200 code 499))

(defn- assoc-access-token-using-code [{:keys [code config] :as ctx}]
  (let [auth0-token-response (client/post (str "https://" (:domain config) (:auth-uri config))
                                          {:form-params {:client_id     (:client-id config)
                                                         :client_secret (:client-secret  config)
                                                         :code          code
                                                         :grant_type    "authorization_code"
                                                         :redirect_uri  (:redirect-uri  config)}
                                           :unexceptional-status unexceptional-status
                                           :as :json})]
    (if (= 200 (:status auth0-token-response))
      (-> ctx
          (assoc-in [:tokens :acces-token] (get-in auth0-token-response [:body :access_token]))
          (assoc-in [:tokens :id-token] (get-in auth0-token-response [:body :id_token]))
          (assoc-in [:tokens :expires-in] (get-in auth0-token-response [:body :expires_in])))

      (throw (ex-info "Auth Error: Cound not obtain access token using code"
                      {:type :auth/code-error
                       :data (select-keys auth0-token-response [:status :body])})))))

(tests
 (def tokens {:acces-token :access-token
              :id-token :id-token,
              :expires-in :expires-in})
 (def request {:code :code
               :config {:domain "domain"
                        :auth-uri "/oauth/token"
                        :client-id :client-id
                        :client-secret :client-secret
                        :redirect-uri :redirect-uri}})
 (def params-http {:form-params {:client_id     :client-id
                                 :client_secret :client-secret
                                 :code          :code
                                 :grant_type    "authorization_code"
                                 :redirect_uri  :redirect-uri}
                   :unexceptional-status unexceptional-status
                   :as :json})
 ;;happy path
 (with-redefs [client/post (spy/spy (fn [_ _] {:status 200 :body {:access_token :access-token
                                                                  :id_token :id-token
                                                                  :expires_in :expires-in}}))]
   (assoc-access-token-using-code request) := (merge request {:tokens tokens})
   (spy/called-once-with? client/post "https://domain/oauth/token" params-http) := true)

 (with-redefs [client/post (spy/spy (fn [_ _] {:status 403 :body {:error "Error"}}))]
   (assoc-access-token-using-code request) :throws clojure.lang.ExceptionInfo
   (spy/called-once-with? client/post "https://domain/oauth/token" params-http) := true))

(defn- assoc-user-info-using-access-token [{:keys [tokens config] :as ctx}]
  (let [user-info-response (client/get (str "https://" (:domain config) "/userinfo") {:headers {"Authorization" (str "Bearer " (:acces-token tokens))}
                                                                                      :unexceptional-status unexceptional-status
                                                                                      :as :json})]

    (if (= 200 (:status user-info-response))
      (assoc ctx :user-info (:body user-info-response))
      ;;else
      (throw (ex-info "Auth Error: User info not obtained based on access-token"
                      {:type :auth/acces-token-error
                       :data (select-keys user-info-response [:status :body])})))))

(tests
 ;;happy path
 (def tokens {:acces-token :access-token
              :id-token :id-token,
              :expires-in :expires-in})
 (def request {:code :code
               :config {:domain "domain"
                        :client-id :client-id
                        :client-secret :client-secret
                        :redirect-uri :redirect-uri}
               :tokens tokens})
 (def params-http {:headers {"Authorization" (str "Bearer " (:acces-token tokens))}
                   :unexceptional-status unexceptional-status
                   :as :json})
 (with-redefs [client/get (spy/spy (fn [_ _] {:status 200 :body {:user-id 1}}))]
   (assoc-user-info-using-access-token request) := (merge request {:user-info {:user-id 1}})
   (spy/called-once-with? client/get "https://domain/userinfo" params-http) := true)

 (with-redefs [client/get (spy/spy (fn [_ _] {:status 403 :body {:error "Error"}}))]
   (assoc-user-info-using-access-token request) :throws clojure.lang.ExceptionInfo
   (spy/called-once-with? client/get "https://domain/userinfo" params-http) := true))

(defn code->token+user-info [{:keys [code config] :as ctx}]
  (-> ctx
      assoc-access-token-using-code
      assoc-user-info-using-access-token))

(tests
 (def tokens {:acces-token :access-token
              :id-token :id-token,
              :expires-in :expires-in})

 (defn- pipe [step]
   (fn [a & _]
     (if (:pipe a)
       (update a :pipe conj step)
       (assoc a :pipe [step]))))

 (def request {:code :code
               :config {:domain "domain"
                        :client-id :client-id
                        :client-secret :client-secret
                        :redirect-uri :redirect-uri}
               :tokens tokens})

 (with-redefs [assoc-access-token-using-code      (spy/spy (pipe :assoc-access-token-using-code))
               assoc-user-info-using-access-token (spy/spy (pipe :assoc-user-info-using-access-token))]
   (code->token+user-info request) := (merge request {:pipe [:assoc-access-token-using-code
                                                             :assoc-user-info-using-access-token]})))


