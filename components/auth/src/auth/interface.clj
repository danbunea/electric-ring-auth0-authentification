(ns auth.interface
  (:require
   [auth.config :as conf]
   [auth.auth-domain :as ad]
   [auth.use-cases.token-from-code.auth0 :as a0]))

(defn create-auth0-login-url [config state]
  (ad/create-auth0-login-url config state))

(defn code->token+user-info [{:keys [code config] :as ctx}]
  (a0/code->token+user-info ctx))

(def auth0-config conf/auth0-config)