(ns auth-middleware.interface
  (:require
   [auth-middleware.adapters.ring.middleware :as mw]))

(defn wrap-auth0-authentication [handler config]
  (mw/wrap-authentication handler config))