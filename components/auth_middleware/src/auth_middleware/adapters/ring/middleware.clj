(ns auth-middleware.adapters.ring.middleware
  (:require
   [hyperfiddle.rcf :refer [tests]]
   [auth.interface :refer [create-auth0-login-url code->token+user-info]]
   [clojure.string :as s]

   [spy.core :as spy]
   [ring.util.codec :refer [form-decode]]))

(defn logged-in? [request]
  (boolean (get-in request [:session :user-info])))

(tests
 (logged-in? {:session {:user-info {:name "John"}}}) := true
 (logged-in? {:session {:user-info nil}}) := false
 (logged-in? {}) := false
 (logged-in? {:session {}}) := false
 (logged-in? {:session {:user-info nil}}) := false)

(defn redirect-to-auth0-handler [config state]
  (fn [request]
    (-> request
        (merge {:status  302
                :headers {"Location" (create-auth0-login-url config state)}}))))

(tests
 (with-redefs [create-auth0-login-url (spy/spy (fn [_ _] "url"))]
   (def func (redirect-to-auth0-handler {} "state"))
   (func {})) := {:status 302 :headers {"Location" "url"}})

(defn new-state [] (str (java.util.UUID/randomUUID)))

(defn redirect-to-auth0 [request config]
  (let [handler (redirect-to-auth0-handler config (new-state))]

    (handler request)))

(tests
 (with-redefs [create-auth0-login-url (spy/spy (fn [_ _] "https://domain.com"))
               new-state (spy/spy (fn [] "state"))]
   (redirect-to-auth0 {:status 200} {:config true}) := {:status 302, :headers {"Location" "https://domain.com"}}))

(defn set-user-in-session-handler [user]
  (fn [request]
    (-> request
        (assoc :session (merge (:session request) {:user-info user})))))

(tests
 (def func (set-user-in-session-handler {:id 1}))
 (func {}) := {:session {:user-info {:id 1}}})

(defn set-user-in-session [request user]
  (let [func (set-user-in-session-handler user)]
    (func request)))

(tests
 (set-user-in-session {} {:id 1}) := {:session {:user-info {:id 1}}})

(defn wrap-authentication [handler config]
  (fn [request]
    (let [logged? (logged-in? request)]
      (if logged?
        (handler request)
          ;;else
        (if (s/ends-with? (:redirect-uri config) (:uri request))
          (let [query-map (form-decode (:query-string request))
                resp (code->token+user-info {:code (get query-map "code")
                                             :config config})]
            (-> request
                handler
                (set-user-in-session (:user-info resp))
                (merge {:status 302
                        :headers {"Location" "/"}})))
            ;;else 
          (redirect-to-auth0 request config))))))

(tests
   ;;not logged, not the /auth/code
 (with-redefs [redirect-to-auth0 (spy/spy (fn [r _] (merge r {:status 302 :headers {"Location" "url"}})))]

      ;;not logged -> redirect
   ((wrap-authentication identity {:redirect-uri "/auth/code"}) {:uri "/"})

   := {:uri "/"
       :status 302
       :headers {"Location" "url"}})

  ;;logged -> execute handler
 (let [handler (fn [_] {:body "" :status 200})]
   ((wrap-authentication handler {:redirect-uri "/auth/code"}) {:uri "/"
                                                                :session {:user-info {:id 1}}}) := {:body "", :status 200})

  ;; receiving the code 
 (with-redefs [code->token+user-info (spy/spy (fn [_] {:user-info {:sub 1}}))]

  ;;not logged -> redirect
   ((wrap-authentication identity {:redirect-uri "/auth/code"})
    {:uri "/auth/code"
     :query-string "code=code&state=state"})

   *1 := {:headers {"Location" "/"},
          :query-string "code=code&state=state",
          :session {:user-info {:sub 1}},
          :status 302,
          :uri "/auth/code"}))


