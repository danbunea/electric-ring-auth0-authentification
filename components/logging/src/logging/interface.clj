(ns logging.interface
  (:require
   [clojure.pprint :refer [pprint]]))

(defn dbg
  ([x y]
   (dbg x y identity))
  ([x y func]
   (println y)
   (pprint (func x))
   x))

