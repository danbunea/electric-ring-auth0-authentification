(ns boot
  (:require
   [ui.todo-list :refer [Todo-list]]
   [hyperfiddle.electric :as e]))

#?(:clj (defn with-ring-request [_ring-req] (e/boot-server {} Todo-list)))
#?(:cljs (def client (e/boot-client {} Todo-list)))